-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 20-05-2024 a las 06:29:45
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.1.30

CREATE DATABASE  IF NOT EXISTS `congressbd` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `congressbd`;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `congressbd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `congress_app`
--

CREATE TABLE `congress_app` (
  `cong_id` int(11) NOT NULL,
  `cong_congressname` varchar(45) DEFAULT NULL,
  `cong_nameapp` text DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `congress_app`
--

INSERT INTO `congress_app` (`cong_id`, `cong_congressname`, `cong_nameapp`, `created_at`, `modified_at`) VALUES
(1, 'Coneic 2024', 'congressApp', '2024-05-18 20:56:40', '2024-05-18 20:56:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrollment`
--

CREATE TABLE `enrollment` (
  `enro_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `part_id` int(11) NOT NULL,
  `insc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `event`
--

CREATE TABLE `event` (
  `even_id` int(11) NOT NULL,
  `even_name` varchar(255) DEFAULT NULL,
  `even_type` enum('TURISMO','FIESTA','SOCIOCULTURAL','CONFERENCIA') DEFAULT NULL,
  `even_theme` enum('ARTE_Y_TECNOLOGIA') DEFAULT NULL,
  `even_description` varchar(255) DEFAULT NULL,
  `even_date` date DEFAULT NULL,
  `even_starttime` time DEFAULT NULL,
  `even_endtime` time DEFAULT NULL,
  `even_labelcolor` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `spea_id` int(11) NOT NULL,
  `loca_id` int(11) NOT NULL,
  `cong_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `event_result`
--

CREATE TABLE `event_result` (
  `evre_id` int(11) NOT NULL,
  `evre_position` varchar(45) DEFAULT NULL,
  `evre_score` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `even_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscription`
--

CREATE TABLE `inscription` (
  `insc_id` int(11) NOT NULL,
  `insc_name` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `even_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `location`
--

CREATE TABLE `location` (
  `loca_id` int(11) NOT NULL,
  `loca_name` varchar(150) DEFAULT NULL,
  `loca_description` varchar(255) DEFAULT NULL,
  `loca_latitude` varchar(45) DEFAULT NULL,
  `loca_longitude` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organizer`
--

CREATE TABLE `organizer` (
  `orga_id` int(11) NOT NULL,
  `orga_name` varchar(150) DEFAULT NULL,
  `orga_lastname` varchar(150) DEFAULT NULL,
  `orga_cellphone` varchar(15) DEFAULT NULL,
  `orga_whatsappnumber` varchar(15) DEFAULT NULL,
  `orga_urlsocialnetwork` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `cong_id` int(11) NOT NULL,
  `orro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organizer_role`
--

CREATE TABLE `organizer_role` (
  `orro_id` int(11) NOT NULL,
  `orro_name` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participant`
--

CREATE TABLE `participant` (
  `part_id` int(11) NOT NULL,
  `part_name` varchar(80) DEFAULT NULL,
  `part_lastname` varchar(80) DEFAULT NULL,
  `part_department` varchar(100) DEFAULT NULL,
  `part_province` varchar(150) DEFAULT NULL,
  `part_district` varchar(200) DEFAULT NULL,
  `part_dni` varchar(12) DEFAULT NULL,
  `part_cellphone` varchar(15) DEFAULT NULL,
  `part_corporativecode` varchar(45) DEFAULT NULL,
  `part_condition` enum('ESTUDIANTE','PROFESIONAL','PUBLICO_GENERAL') DEFAULT NULL,
  `part_promotion` enum('CORPORATIVO','BECADO','PROMOCION','NORMAL') DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  `univ_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment`
--

CREATE TABLE `payment` (
  `paym_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `insc_id` int(11) NOT NULL,
  `usro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(40) NOT NULL,
  `role_type` enum('SUPERADMIN','ADMIN','SELLER','ATTENDANCE_ASSISTANT','PARTICIPANT') DEFAULT NULL,
  `role_status` char(1) DEFAULT '1',
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `role_type`, `role_status`, `created_at`, `modified_at`) VALUES
(1, 'Super Administrador', 'SUPERADMIN', '1', '2024-05-18 21:48:54', '2024-05-18 21:48:54'),
(2, 'Administrador', 'ADMIN', '1', '2024-05-18 22:01:25', '2024-05-18 22:07:10'),
(3, 'Vendedor', 'SELLER', '1', '2024-05-18 22:02:08', '2024-05-18 22:02:08'),
(4, 'Asistente de asistencia', 'ATTENDANCE_ASSISTANT', '1', '2024-05-18 22:03:16', '2024-05-18 22:03:16'),
(5, 'Participante', 'PARTICIPANT', '1', '2024-05-18 22:03:28', '2024-05-18 22:03:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `speaker`
--

CREATE TABLE `speaker` (
  `spea_id` int(11) NOT NULL,
  `spea_name` varchar(45) DEFAULT NULL,
  `spea_lastname` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sponsor`
--

CREATE TABLE `sponsor` (
  `spon_id` int(11) NOT NULL,
  `spon_name` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `cong_id` int(11) NOT NULL,
  `spty_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sponsor_type`
--

CREATE TABLE `sponsor_type` (
  `spty_id` int(11) NOT NULL,
  `spty_name` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `university`
--

CREATE TABLE `university` (
  `univ_id` int(11) NOT NULL,
  `univ_name` varchar(80) DEFAULT NULL,
  `univ_acronym` varchar(45) DEFAULT NULL,
  `univ_location` varchar(100) DEFAULT NULL,
  `univ_status` char(1) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `university`
--

INSERT INTO `university` (`univ_id`, `univ_name`, `univ_acronym`, `univ_location`, `univ_status`, `created_at`, `modified_at`) VALUES
(1, 'Universidad Nacional Mayor de San Marcos', 'UNMSM', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(2, 'Universidad Nacional de San Cristóbal de Huamanga', 'UNSCH', 'Ayacucho', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(3, 'Universidad Nacional San Antonio Abad del Cusco', 'UNSAAC', 'Cuzco', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(4, 'Universidad Nacional de Educación Enrique Guzmán y Valle', 'UNE', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(5, 'Universidad Nacional de Trujillo', 'UNT', 'Trujillo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(6, 'Universidad Nacional de San Agustín', 'UNSA', 'Arequipa', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(7, 'Universidad Nacional del Altiplano de Puno', 'UNAP', 'Puno', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(8, 'Universidad Nacional de Ingeniería', 'UNI', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(9, 'Universidad Nacional Agraria La Molina', 'UNALM', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(10, 'Universidad Nacional San Luis Gonzaga', 'UNICA', 'Ica', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(11, 'Universidad Nacional del Centro del Perú', 'UNCP', 'Huancayo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(12, 'Universidad Nacional Daniel Alcides Carrión', 'UNDAC', 'Cerro de Pasco', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(13, 'Universidad Nacional Hermilio Valdizán', 'UNHEVAL', 'Huánuco', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(14, 'Universidad Nacional de la Amazonía Peruana', 'UNAP', 'Iquitos', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(15, 'Universidad Nacional de Piura', 'UNP', 'Piura', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(16, 'Universidad Nacional de Cajamarca', 'UNC', 'Cajamarca', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(17, 'Universidad Nacional Federico Villarreal', 'UNFV', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(18, 'Universidad Nacional Agraria de la Selva', 'UNAS', 'Tingo María', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(19, 'Universidad Nacional del Callao', 'UNAC', 'Callao', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(20, 'Universidad Nacional José Faustino Sánchez Carrión', 'UNJFSC', 'Huacho', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(21, 'Universidad Nacional Pedro Ruiz Gallo', 'UNPRG', 'Lambayeque', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(22, 'Universidad Nacional Jorge Basadre Grohmann', 'UNJBG', 'Tacna', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(23, 'Universidad Nacional Santiago Antúnez de Mayolo', 'UNASAM', 'Huaraz', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(24, 'Universidad Nacional de Ucayali', 'UNU', 'Pucallpa', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(25, 'Universidad Nacional de San Martín', 'UNSM', 'Tarapoto', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(26, 'Universidad Nacional del Santa', 'UNS', 'Chimbote', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(27, 'Universidad Nacional de Tumbes', 'UNT', 'Tumbes', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(28, 'Universidad Nacional de Huancavelica', 'UNH', 'Huancavelica', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(29, 'Universidad Nacional Amazónica de Madre de Dios', 'UNAMAD', 'Puerto Maldonado', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(30, 'Universidad Nacional Intercultural de la Amazonía', 'UNIA', 'Yarinacocha', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(31, 'Universidad Nacional Micaela Bastidas de Apurímac', 'UNAMBA', 'Abancay', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(32, 'Universidad Nacional Toribio Rodríguez de Mendoza de Amazonas', 'UNTRMA', 'Chachapoyas', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(33, 'Universidad Nacional Tecnológica de Lima Sur', 'UNTELS', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(34, 'Universidad Nacional José María Arguedas', 'UNAJMA', 'Andahuaylas', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(35, 'Universidad Nacional de Moquegua', 'UNM', 'Moquegua', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(36, 'Universidad Nacional de Juliaca', 'UNAJ', 'Juliaca', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(37, 'Universidad Nacional Autónoma Altoandina de Tarma', 'UNAAT', 'Tarma', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(38, 'Universidad Nacional Autónoma de Chota', 'UNACh', 'Chota', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(39, 'Universidad Nacional de Frontera', 'UNFS', 'Sullana', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(40, 'Universidad Nacional Intercultural de la Selva Central Juan Santos Atahualpa', 'UNISCJSA', 'Chanchamayo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(41, 'Universidad Nacional Intercultural Fabiola Salazar Leguía de Bagua', 'UNIBAGUA', 'Bagua', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(42, 'Universidad Nacional de Barranca', 'UNAB', 'Barranca', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(43, 'Universidad Nacional Autónoma de Huanta', 'UNAH', 'Huanta', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(44, 'Universidad Nacional de Jaén', 'UNJ', 'Jaén', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(45, 'Universidad Nacional Autónoma de Alto Amazonas', 'UNAAA', 'Yurimaguas', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(46, 'Universidad Nacional Ciro Alegría', 'UNCA', 'Huamachuco', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(47, 'Universidad Nacional Autónoma de Tayacaja Daniel Hernández Morillo', 'UNAT', 'Tayacaja', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(48, 'Universidad Nacional de Cañete', 'UNDC', 'Cañete', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(49, 'Universidad Nacional Intercultural de Quillabamba', 'UNIQ', 'Quillabamba', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(50, 'Pontificia Universidad Católica del Perú', 'PUCP', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(51, 'Universidad Peruana Cayetano Heredia', 'UPCH', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(52, 'Universidad Católica de Santa María', 'UCSM', 'Arequipa', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(53, 'Universidad del Pacífico', 'UP', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(54, 'Universidad de Lima', 'ULIMA', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(55, 'Universidad de San Martín de Porres', 'USMP', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(56, 'Universidad Femenina del Sagrado Corazón', 'UNIFÉ', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(57, 'Universidad Inca Garcilaso de la Vega', 'UIGV', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(58, 'Universidad Marcelino Champagnat', 'UMCH', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(59, 'Universidad de Piura', 'UDEP', 'Piura', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(60, 'Universidad Ricardo Palma', 'URP', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(61, 'Universidad Andina del Cusco', 'UAC', 'Cuzco', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(62, 'Universidad Peruana Los Andes', 'UPLA', 'Huancayo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(63, 'Universidad Peruana Unión', 'UPeU', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(64, 'Universidad Tecnológica de los Andes', 'UTEA', 'Abancay', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(65, 'Universidad de Huánuco', 'UDH', 'Huánuco', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(66, 'Universidad Privada de Tacna', 'UPT', 'Tacna', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(67, 'Universidad Privada Antenor Orrego', 'UPAO', 'Trujillo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(68, 'Universidad Particular de Iquitos', 'UPI', 'Iquitos', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(69, 'Universidad César Vallejo', 'UCV', 'Trujillo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(70, 'Universidad Privada del Norte', 'UPN', 'Trujillo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(71, 'Facultad de Teología Pontificia y Civil de Lima', 'FTPCL', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(72, 'Universidad Peruana de Ciencias Aplicadas', 'UPC', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(73, 'Universidad San Ignacio de Loyola', 'USIL', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(74, 'Universidad Católica Santo Toribio de Mogrovejo', 'USAT', 'Chiclayo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(75, 'Universidad Norbert Wiener', 'UW', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(76, 'Universidad Católica San Pablo', 'UCSP', 'Arequipa', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(77, 'Universidad Privada San Juan Bautista', 'UPSBJ', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(78, 'Universidad Tecnológica del Perú', 'UTP', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(79, 'Universidad Católica Sedes Sapientiae', 'UCSS', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(80, 'Universidad Científica del Sur', 'UCSur', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(81, 'Universidad Continental', 'UC', 'Huancayo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(82, 'Escuela de Postgrado Gerens', 'Gerens', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(83, 'Universidad Señor de Sipán', 'USS', 'Chiclayo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(84, 'Universidad Cátólica de Trujillo Benedicto XVI', 'UCT', 'Trujillo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(85, 'Universidad para el Desarrollo Andino', 'UDeA', 'Lircay', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(86, 'Universidad Antonio Ruiz de Montoya', 'UARM', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(87, 'Universidad ESAN', 'UE', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(88, 'Universidad Jaime Bausate y Meza', 'UJBM', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(89, 'Universidad de Ciencias y Humanidades', 'UCH', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(90, 'Universidad Autónoma de Ica', 'UAI', 'Ica', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(91, 'Universidad Autónoma del Perú', 'UA', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(92, 'Universidad de Ciencias y Artes de América Latina', 'UCAL', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(93, 'Universidad La Salle', 'Ulasalle', 'Arequipa', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(94, 'Universidad Privada de Huancayo Franklin Roosevelt', 'UPHFR', 'Huancayo', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(95, 'Universidad de Ingeniería y Tecnología', 'UTEC', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(96, 'Universidad María Auxiliadora', 'UMA', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(97, 'Universidad Privada Peruano Alemana', 'UPAL', 'Lima', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(98, 'Escuela de Postgrado Neumann Business School', 'Neumann', 'Tacna', '1', '2024-05-15 16:47:45', '2024-05-15 16:47:45'),
(107, 'Universidad Sideral Carrion', 'USC', 'Lima', '0', '2024-05-18 17:57:35', '2024-05-18 17:57:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(16) NOT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `cong_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`user_id`, `user_username`, `user_email`, `user_password`, `created_at`, `modified_at`, `cong_id`) VALUES
(1, 'admin', 'admin@gmail.com', '$2a$10$GN2LkkAIIEEmhZM/P13FM.dH.Jv2nlGBABWsPNAIPePoZ3N.wXLTG', '2024-05-19 23:12:17', '2024-05-19 23:12:17', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_has_role`
--

CREATE TABLE `user_has_role` (
  `usro_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user_has_role`
--

INSERT INTO `user_has_role` (`usro_id`, `created_at`, `modified_at`, `role_id`, `user_id`) VALUES
(1, '2024-05-19 23:12:17', '2024-05-19 23:12:17', 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `congress_app`
--
ALTER TABLE `congress_app`
  ADD PRIMARY KEY (`cong_id`),
  ADD KEY `co_congressname_idx` (`cong_congressname`);

--
-- Indices de la tabla `enrollment`
--
ALTER TABLE `enrollment`
  ADD PRIMARY KEY (`enro_id`),
  ADD KEY `fk_participant_has_inscription_inscription1_idx` (`insc_id`),
  ADD KEY `fk_participant_has_inscription_participant1_idx` (`part_id`);

--
-- Indices de la tabla `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`even_id`),
  ADD KEY `fk_event_speaker1_idx` (`spea_id`),
  ADD KEY `fk_event_congress_app1_idx` (`cong_id`),
  ADD KEY `fk_event_location1_idx` (`loca_id`),
  ADD KEY `ev_name_idx` (`even_name`),
  ADD KEY `ev_type_idx` (`even_type`),
  ADD KEY `ev_theme_idx` (`even_theme`),
  ADD KEY `ev_date_idx` (`even_date`),
  ADD KEY `ev_natythda_dx` (`even_name`,`even_type`,`even_theme`,`even_date`);

--
-- Indices de la tabla `event_result`
--
ALTER TABLE `event_result`
  ADD PRIMARY KEY (`evre_id`),
  ADD KEY `fk_event_result_event1_idx` (`even_id`),
  ADD KEY `er_position_score_idx` (`evre_position`,`evre_score`);

--
-- Indices de la tabla `inscription`
--
ALTER TABLE `inscription`
  ADD PRIMARY KEY (`insc_id`),
  ADD KEY `fk_inscription_event1_idx` (`even_id`);

--
-- Indices de la tabla `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`loca_id`),
  ADD KEY `lo_name_idx` (`loca_name`);

--
-- Indices de la tabla `organizer`
--
ALTER TABLE `organizer`
  ADD PRIMARY KEY (`orga_id`),
  ADD KEY `fk_organizer_congress_app1_idx` (`cong_id`),
  ADD KEY `fk_organizer_organizer_role1_idx` (`orro_id`),
  ADD KEY `or_name_lastname_idx` (`orga_name`,`orga_lastname`);

--
-- Indices de la tabla `organizer_role`
--
ALTER TABLE `organizer_role`
  ADD PRIMARY KEY (`orro_id`),
  ADD KEY `ro_name_idx` (`orro_name`);

--
-- Indices de la tabla `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`part_id`),
  ADD KEY `fk_participant_university1_idx` (`univ_id`),
  ADD KEY `pa_name_lastname_idx` (`part_name`,`part_lastname`),
  ADD KEY `pa_dni_idx` (`part_dni`),
  ADD KEY `pa_condition_idx` (`part_condition`),
  ADD KEY `pa_promotion_idx` (`part_promotion`);

--
-- Indices de la tabla `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`paym_id`),
  ADD KEY `fk_inscription_has_user_has_role_user_has_role1_idx` (`usro_id`),
  ADD KEY `fk_inscription_has_user_has_role_inscription1_idx` (`insc_id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`),
  ADD KEY `ro_name_idx` (`role_name`);

--
-- Indices de la tabla `speaker`
--
ALTER TABLE `speaker`
  ADD PRIMARY KEY (`spea_id`),
  ADD KEY `sp_name_idx` (`spea_name`);

--
-- Indices de la tabla `sponsor`
--
ALTER TABLE `sponsor`
  ADD PRIMARY KEY (`spon_id`),
  ADD KEY `fk_sponsor_congress_app1_idx` (`cong_id`),
  ADD KEY `fk_sponsor_sponsor_type1_idx` (`spty_id`),
  ADD KEY `sp_name_idx` (`spon_name`);

--
-- Indices de la tabla `sponsor_type`
--
ALTER TABLE `sponsor_type`
  ADD PRIMARY KEY (`spty_id`),
  ADD KEY `st_name_idx` (`spty_name`);

--
-- Indices de la tabla `university`
--
ALTER TABLE `university`
  ADD PRIMARY KEY (`univ_id`),
  ADD KEY `un_name_idx` (`univ_name`),
  ADD KEY `un_acronym_idx` (`univ_acronym`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `fk_user_congress_app1_idx` (`cong_id`),
  ADD KEY `us_email_username_idx` (`user_email`,`user_username`);

--
-- Indices de la tabla `user_has_role`
--
ALTER TABLE `user_has_role`
  ADD PRIMARY KEY (`usro_id`),
  ADD KEY `fk_user_has_role_role1_idx` (`role_id`),
  ADD KEY `fk_user_has_role_user_idx` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `congress_app`
--
ALTER TABLE `congress_app`
  MODIFY `cong_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `enrollment`
--
ALTER TABLE `enrollment`
  MODIFY `enro_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `event`
--
ALTER TABLE `event`
  MODIFY `even_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `event_result`
--
ALTER TABLE `event_result`
  MODIFY `evre_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inscription`
--
ALTER TABLE `inscription`
  MODIFY `insc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `location`
--
ALTER TABLE `location`
  MODIFY `loca_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `organizer`
--
ALTER TABLE `organizer`
  MODIFY `orga_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `organizer_role`
--
ALTER TABLE `organizer_role`
  MODIFY `orro_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `participant`
--
ALTER TABLE `participant`
  MODIFY `part_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `speaker`
--
ALTER TABLE `speaker`
  MODIFY `spea_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sponsor`
--
ALTER TABLE `sponsor`
  MODIFY `spon_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sponsor_type`
--
ALTER TABLE `sponsor_type`
  MODIFY `spty_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `university`
--
ALTER TABLE `university`
  MODIFY `univ_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user_has_role`
--
ALTER TABLE `user_has_role`
  MODIFY `usro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `enrollment`
--
ALTER TABLE `enrollment`
  ADD CONSTRAINT `fk_participant_has_inscription_inscription1` FOREIGN KEY (`insc_id`) REFERENCES `inscription` (`insc_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_participant_has_inscription_participant1` FOREIGN KEY (`part_id`) REFERENCES `participant` (`part_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_event_congress_app1` FOREIGN KEY (`cong_id`) REFERENCES `congress_app` (`cong_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_event_location1` FOREIGN KEY (`loca_id`) REFERENCES `location` (`loca_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_event_speaker1` FOREIGN KEY (`spea_id`) REFERENCES `speaker` (`spea_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `event_result`
--
ALTER TABLE `event_result`
  ADD CONSTRAINT `fk_event_result_event1` FOREIGN KEY (`even_id`) REFERENCES `event` (`even_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inscription`
--
ALTER TABLE `inscription`
  ADD CONSTRAINT `fk_inscription_event1` FOREIGN KEY (`even_id`) REFERENCES `event` (`even_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `organizer`
--
ALTER TABLE `organizer`
  ADD CONSTRAINT `fk_organizer_congress_app1` FOREIGN KEY (`cong_id`) REFERENCES `congress_app` (`cong_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_organizer_organizer_role1` FOREIGN KEY (`orro_id`) REFERENCES `organizer_role` (`orro_id`);

--
-- Filtros para la tabla `participant`
--
ALTER TABLE `participant`
  ADD CONSTRAINT `fk_participant_university1` FOREIGN KEY (`univ_id`) REFERENCES `university` (`univ_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_inscription_has_user_has_role_inscription1` FOREIGN KEY (`insc_id`) REFERENCES `inscription` (`insc_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inscription_has_user_has_role_user_has_role1` FOREIGN KEY (`usro_id`) REFERENCES `user_has_role` (`usro_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sponsor`
--
ALTER TABLE `sponsor`
  ADD CONSTRAINT `fk_sponsor_congress_app1` FOREIGN KEY (`cong_id`) REFERENCES `congress_app` (`cong_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sponsor_sponsor_type1` FOREIGN KEY (`spty_id`) REFERENCES `sponsor_type` (`spty_id`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_congress_app1` FOREIGN KEY (`cong_id`) REFERENCES `congress_app` (`cong_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_has_role`
--
ALTER TABLE `user_has_role`
  ADD CONSTRAINT `fk_user_has_role_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_role_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
