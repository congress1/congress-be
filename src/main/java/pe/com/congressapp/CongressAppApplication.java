package pe.com.congressapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CongressAppApplication {

  public static void main(String[] args) {
    SpringApplication.run(CongressAppApplication.class, args);
  }
}
