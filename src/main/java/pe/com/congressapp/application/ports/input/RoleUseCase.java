package pe.com.congressapp.application.ports.input;

import java.util.List;
import java.util.Optional;
import pe.com.congressapp.domain.Role;
import pe.com.congressapp.domain.models.request.RoleRequest;

public interface RoleUseCase {

  List<Role> getRoles();

  Role saveRole(RoleRequest roleRequest);

  Optional<Role> updateRole(RoleRequest roleRequest);

  void updateStatusRole(Long id);
}
