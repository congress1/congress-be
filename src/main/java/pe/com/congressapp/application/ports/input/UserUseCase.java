package pe.com.congressapp.application.ports.input;

import pe.com.congressapp.domain.models.request.LoginRequest;
import pe.com.congressapp.domain.models.request.RegisterRequest;

public interface UserUseCase {
  String saveUser(RegisterRequest request);

  String login(LoginRequest request);
}
