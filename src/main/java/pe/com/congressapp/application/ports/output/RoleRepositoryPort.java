package pe.com.congressapp.application.ports.output;

import java.util.List;
import java.util.Optional;
import pe.com.congressapp.domain.Role;
import pe.com.congressapp.domain.models.request.RoleRequest;

public interface RoleRepositoryPort {

  List<Role> getListRoles();

  Role saveRole(RoleRequest roleRequest);

  Optional<Role> updateRole(RoleRequest roleRequest);

  void updateStatusRole(Long id);
}
