package pe.com.congressapp.application.ports.output;

import java.util.List;
import java.util.Optional;
import pe.com.congressapp.domain.Speaker;
import pe.com.congressapp.domain.models.request.SpeakerRequest;

public interface SpeakerRepositoryPort {

  List<Speaker> getListSpeakers();

  Speaker saveSpeaker(SpeakerRequest speakerRequest);

  Optional<Speaker> updateSpeaker(SpeakerRequest speakerRequest);

  void updateStatusSpeaker(Long id);
}
