package pe.com.congressapp.application.ports.output;

import java.util.List;
import java.util.Optional;
import pe.com.congressapp.domain.University;
import pe.com.congressapp.domain.models.request.UniversityRequest;

public interface UniversityRepositoryPort {

  List<University> getListUniversities();

  University saveUniversity(UniversityRequest universityRequest);

  Optional<University> updateUniversity(UniversityRequest universityRequest);

  void updateStatusUniversity(Long id);
}
