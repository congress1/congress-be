package pe.com.congressapp.application.ports.output;

import java.util.Optional;
import pe.com.congressapp.domain.entities.UserEntity;
import pe.com.congressapp.domain.models.request.RegisterRequest;

public interface UserRepositoryPort {
  UserEntity saveUser(RegisterRequest request);

  Optional<UserEntity> getUser(String username);
}
