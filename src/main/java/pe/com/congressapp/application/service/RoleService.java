package pe.com.congressapp.application.service;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pe.com.congressapp.application.ports.input.RoleUseCase;
import pe.com.congressapp.application.ports.output.RoleRepositoryPort;
import pe.com.congressapp.domain.Role;
import pe.com.congressapp.domain.models.request.RoleRequest;

@Slf4j
@Service
@RequiredArgsConstructor
public class RoleService implements RoleUseCase {

  private final RoleRepositoryPort roleRepositoryPort;

  @Override
  public List<Role> getRoles() {
    return roleRepositoryPort.getListRoles();
  }

  @Override
  public Role saveRole(RoleRequest roleRequest) {
    return roleRepositoryPort.saveRole(roleRequest);
  }

  @Override
  public Optional<Role> updateRole(RoleRequest roleRequest) {
    return roleRepositoryPort.updateRole(roleRequest);
  }

  @Override
  public void updateStatusRole(Long id) {
    roleRepositoryPort.updateStatusRole(id);
  }
}
