package pe.com.congressapp.application.service;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pe.com.congressapp.application.ports.input.SpeakerUseCase;
import pe.com.congressapp.application.ports.output.SpeakerRepositoryPort;
import pe.com.congressapp.domain.Speaker;
import pe.com.congressapp.domain.models.request.SpeakerRequest;

@Slf4j
@Service
@RequiredArgsConstructor
public class SpeakerService implements SpeakerUseCase {

  private final SpeakerRepositoryPort speakerRepositoryPort;

  @Override
  public List<Speaker> getSpeakers() {
    return speakerRepositoryPort.getListSpeakers();
  }

  @Override
  public Speaker saveSpeaker(SpeakerRequest speakerRequest) {
    return speakerRepositoryPort.saveSpeaker(speakerRequest);
  }

  @Override
  public Optional<Speaker> updateSpeaker(SpeakerRequest speakerRequest) {
    return speakerRepositoryPort.updateSpeaker(speakerRequest);
  }

  @Override
  public void updateStatusSpeaker(Long id) {
    speakerRepositoryPort.updateStatusSpeaker(id);
  }
}
