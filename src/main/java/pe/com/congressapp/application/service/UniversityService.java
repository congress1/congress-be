package pe.com.congressapp.application.service;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pe.com.congressapp.application.ports.input.UniversityUseCase;
import pe.com.congressapp.application.ports.output.UniversityRepositoryPort;
import pe.com.congressapp.domain.University;
import pe.com.congressapp.domain.models.request.UniversityRequest;

@Slf4j
@Service
@RequiredArgsConstructor
public class UniversityService implements UniversityUseCase {

  private final UniversityRepositoryPort universityRepositoryPort;

  @Override
  public List<University> getUniversities() {
    return universityRepositoryPort.getListUniversities();
  }

  @Override
  public University saveUniversity(UniversityRequest universityRequest) {
    return universityRepositoryPort.saveUniversity(universityRequest);
  }

  @Override
  public Optional<University> updateUniversity(UniversityRequest universityRequest) {
    return universityRepositoryPort.updateUniversity(universityRequest);
  }

  @Override
  public void updateStatusUniversity(Long id) {
    universityRepositoryPort.updateStatusUniversity(id);
  }
}
