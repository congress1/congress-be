package pe.com.congressapp.application.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pe.com.congressapp.application.ports.input.UserUseCase;
import pe.com.congressapp.application.ports.output.UserRepositoryPort;
import pe.com.congressapp.domain.entities.UserEntity;
import pe.com.congressapp.domain.models.request.LoginRequest;
import pe.com.congressapp.domain.models.request.RegisterRequest;
import pe.com.congressapp.infrastructure.config.jwt.JwtService;

@Service
@RequiredArgsConstructor
public class UserService implements UserUseCase {
  private final UserRepositoryPort registerRepositoryPort;
  private final AuthenticationManager authenticationManager;
  private final JwtService jwtService;

  @Override
  public String saveUser(RegisterRequest request) {
    UserEntity user = registerRepositoryPort.saveUser(request);
    return jwtService.getToken(user);
  }

  @Override
  public String login(LoginRequest request) {
    UserEntity user;
    try {
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
      user = registerRepositoryPort.getUser(request.getUsername()).orElse(null);
    } catch (BadCredentialsException | UsernameNotFoundException ex) {
      registerRepositoryPort
          .getUser(request.getUsername())
          .orElseThrow(() -> new UsernameNotFoundException("Incorrect User / Email"));
      throw new BadCredentialsException("Incorrect password");
    }
    return jwtService.getToken(user);
  }
}
