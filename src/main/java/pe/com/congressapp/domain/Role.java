package pe.com.congressapp.domain;

import lombok.Builder;
import lombok.Getter;
import pe.com.congressapp.domain.constants.RoleType;

@Builder
@Getter
public class Role {

  private String id;
  private String name;
  private RoleType type;
}
