package pe.com.congressapp.domain;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Speaker {

  private String id;
  private String name;
  private String lastName;
  private String status;
  private String urlSocialNetwork;
  private String description;
}
