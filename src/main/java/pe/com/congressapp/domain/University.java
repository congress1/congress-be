package pe.com.congressapp.domain;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class University {

  private String id;
  private String name;
  private String acronym;
  private String location;
  private String status;
}
