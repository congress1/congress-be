package pe.com.congressapp.domain.constants;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EventType {
  TURISMO("TURISMO"),
  FIESTA("FIESTA"),
  SOCIOCULTURAL("SOCIOCULTURAL"),
  CONFERENCIA("CONFERENCIA"),
  NONE("None");

  private final String description;

  public static EventType getEventType(String description) {
    return Arrays.stream(values())
        .filter(it -> it.getDescription().equalsIgnoreCase(description))
        .findFirst()
        .orElse(EventType.NONE);
  }
}
