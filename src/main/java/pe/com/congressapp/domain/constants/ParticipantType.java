package pe.com.congressapp.domain.constants;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ParticipantType {
  ESTUDIANTE("ESTUDIANTE"),
  PROFESIONAL("PROFESIONAL"),
  PUBLICO_GENERAL("PUBLICO_GENERAL"),
  NONE("NONE");

  private final String description;

  public static ParticipantType getParticipantType(String description) {
    return Arrays.stream(values())
        .filter(it -> it.getDescription().equalsIgnoreCase(description))
        .findFirst()
        .orElse(ParticipantType.NONE);
  }
}
