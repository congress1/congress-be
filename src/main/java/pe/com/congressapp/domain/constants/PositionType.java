package pe.com.congressapp.domain.constants;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PositionType {
  PRESIDENTE("PRESIDENTE"),
  VICEPRESIDENTE("VICEPRESIDENTE"),
  SECRETARIO("SECRETARIO"),
  TESORERO("TESORERO"),
  DIRECTOR_AREA_ACADEMICA("DIRECTOR_AREA_ACADEMICA"),
  DIRECTOR_AREA_SOCIO_CULTURAL("DIRECTOR_AREA_SOCIO_CULTURAL"),
  DIRECTOR_AREA_CONTROL("DIRECTOR_AREA_CONTROL"),
  NONE("NONE");

  private final String description;

  public static PositionType getPositionType(String description) {
    return Arrays.stream(values())
        .filter(it -> it.getDescription().equalsIgnoreCase(description))
        .findFirst()
        .orElse(PositionType.NONE);
  }
}
