package pe.com.congressapp.domain.constants;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PromotionType {
  CORPORATIVO("CORPORATIVO"),
  BECADO("BECADO"),
  PROMOCION("PROMOCION"),
  NORMAL("NORMAL"),
  NONE("NONE");

  private final String description;

  public static PromotionType getPromotionType(String description) {
    return Arrays.stream(values())
        .filter(it -> it.getDescription().equalsIgnoreCase(description))
        .findFirst()
        .orElse(PromotionType.NONE);
  }
}
