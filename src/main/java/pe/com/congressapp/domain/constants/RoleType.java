package pe.com.congressapp.domain.constants;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleType {
  SUPERADMIN("SUPERADMIN"),
  ADMIN("ADMIN"),
  SELLER("SELLER"),
  ATTENDANCE_ASSISTANT("ATTENDANCE_ASSISTANT"),
  PARTICIPANT("PARTICIPANT"),
  NONE("NONE");

  private final String description;

  public static RoleType getRoleType(String description) {
    return Arrays.stream(values())
        .filter(it -> it.getDescription().equalsIgnoreCase(description))
        .findFirst()
        .orElse(RoleType.NONE);
  }
}
