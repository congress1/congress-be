package pe.com.congressapp.domain.constants;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SponsorType {
  ORO("ORO"),
  BRONCE("BRONCE"),
  PLATA("PLATA"),
  COLABORADOR("COLABORADOR"),
  ORGANIZADOR("ORGANIZADOR"),
  NONE("NONE");

  private final String description;

  public static SponsorType getSponsorType(String description) {
    return Arrays.stream(values())
        .filter(it -> it.getDescription().equalsIgnoreCase(description))
        .findFirst()
        .orElse(SponsorType.NONE);
  }
}
