package pe.com.congressapp.domain.constants;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ThemeType {
  ARTE_Y_TECNOLOGIA("ARTE_Y_TECNOLOGIA"),
  NONE("NONE");

  private final String description;

  public static ThemeType getThemeType(String description) {
    return Arrays.stream(values())
        .filter(it -> it.getDescription().equalsIgnoreCase(description))
        .findFirst()
        .orElse(ThemeType.NONE);
  }
}
