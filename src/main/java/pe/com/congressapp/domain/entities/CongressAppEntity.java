package pe.com.congressapp.domain.entities;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CongressAppEntity {

  private Long id;
  private String name;
  private String nameApp;
  private String aboutUs;
  private Date createdAt;
  private Date modifiedAt;
}
