package pe.com.congressapp.domain.entities;

import java.time.LocalTime;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import pe.com.congressapp.domain.constants.EventType;
import pe.com.congressapp.domain.constants.ThemeType;

@Getter
@Setter
public class EventEntity {

  private Long id;
  private String name;
  private EventType eventType;
  private ThemeType theme;
  private String description;
  private Date date;
  private LocalTime startTime;
  private LocalTime endTime;
  private String labelColor;
  private Date createdAt;
  private Date modifiedAt;
}
