package pe.com.congressapp.domain.entities;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationEntity {

  private Long id;
  private String name;
  private String description;
  private String latitude;
  private String longitude;
  private String urlImage;
  private Date createdAt;
  private Date modifiedAt;
}
