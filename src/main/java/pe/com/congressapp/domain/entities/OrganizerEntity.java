package pe.com.congressapp.domain.entities;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import pe.com.congressapp.domain.constants.PositionType;

@Getter
@Setter
public class OrganizerEntity {

  private String name;
  private String lastName;
  private PositionType position;
  private String cellPhone;
  private String whatsappNumber;
  private String urlSocialNetwork;
  private Date createdAt;
  private Date modifiedAt;
  private Long congId;
}
