package pe.com.congressapp.domain.entities;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import pe.com.congressapp.domain.constants.ParticipantType;
import pe.com.congressapp.domain.constants.PromotionType;

@Getter
@Setter
public class ParticipantEntity {

  private Long id;
  private String name;
  private String lastName;
  private String department;
  private String province;
  private String district;
  private String dni;
  private String cellPhone;
  private ParticipantType condition;
  private PromotionType promotion;
  private String corporativeCode;
  private Date createdAt;
  private Date modifiedAt;
  private Long univId;
}
