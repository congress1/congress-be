package pe.com.congressapp.domain.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "speaker")
public class SpeakerEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "spea_id")
  private Long id;

  @Column(name = "spea_name")
  private String name;

  @Column(name = "spea_lastname")
  private String lastName;

  @Column(name = "spea_urlsocialnetwork")
  private String urlSocialNetwork;

  @Column(name = "spea_description")
  private String description;

  @Column(name = "spea_nationality")
  private String nationality;

  @Column(name = "spea_status")
  private String status;

  @Column(name = "created_at")
  private Date createdAt;

  @Column(name = "modified_at")
  private Date modifiedAt;
}
