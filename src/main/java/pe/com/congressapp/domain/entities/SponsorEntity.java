package pe.com.congressapp.domain.entities;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import pe.com.congressapp.domain.constants.SponsorType;

@Getter
@Setter
public class SponsorEntity {

  private Long id;
  private String name;
  private SponsorType sponsorType;
  private String description;
  private String urlSocialNetwork;
  private String urlImage;
  private Date createdAt;
  private Date modifiedAt;
}
