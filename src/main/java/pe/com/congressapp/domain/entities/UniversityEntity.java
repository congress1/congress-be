package pe.com.congressapp.domain.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "university")
public class UniversityEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "univ_id")
  private Long id;

  @Column(name = "univ_name")
  private String name;

  @Column(name = "univ_acronym")
  private String acronym;

  @Column(name = "univ_location")
  private String location;

  @Column(name = "univ_status")
  private String status;

  @Column(name = "created_at")
  private Date createdAt;

  @Column(name = "modified_at")
  private Date modifiedAt;
}
