package pe.com.congressapp.domain.exception;

public class ConflictException extends RuntimeException {

  public ConflictException(String detail) {
    super(detail);
  }
}
