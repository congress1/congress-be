package pe.com.congressapp.domain.exception;

public class NotFoundException extends RuntimeException {

  public NotFoundException(String detail) {
    super(detail);
  }
}
