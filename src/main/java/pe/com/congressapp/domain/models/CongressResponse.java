package pe.com.congressapp.domain.models;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import pe.com.congressapp.domain.University;

@Builder
@Getter
public class CongressResponse implements Serializable {

  @Serial private static final long serialVersionUID = 1L;

  private List<University> data;
}
