package pe.com.congressapp.domain.models.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterRequest implements Serializable {
  @NotNull(message = "username cannot be null")
  @NotBlank(message = "username cannot be blank")
  private String username;

  @NotNull(message = "password cannot be null")
  @NotBlank(message = "password cannot be blank")
  private String password;

  @NotNull(message = "email cannot be null")
  @NotBlank(message = "email cannot be blank")
  @Email(message = "email address must be a correctly formatted")
  private String email;
}
