package pe.com.congressapp.domain.models.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.io.Serial;
import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RoleRequest implements Serializable {

  @Serial private static final long serialVersionUID = 1L;

  private Long id;

  @NotNull(message = "name cannot be null")
  @NotBlank(message = "name cannot be blank")
  private String name;

  @NotNull(message = "type cannot be null")
  @NotBlank(message = "type cannot be blank")
  @Pattern(
      regexp = "SUPERADMIN|ADMIN|SELLER|ATTENDANCE_ASSISTANT|PARTICIPANT",
      message = "type must be one of SUPERADMIN, ADMIN, SELLER, ATTENDANCE_ASSISTANT, PARTICIPANT")
  private String type;
}
