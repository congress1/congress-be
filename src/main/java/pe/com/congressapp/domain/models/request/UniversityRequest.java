package pe.com.congressapp.domain.models.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.io.Serial;
import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UniversityRequest implements Serializable {

  @Serial private static final long serialVersionUID = 1L;

  private Long id;

  @NotNull(message = "name cannot be null")
  @NotBlank(message = "name cannot be blank")
  private String name;

  @NotNull(message = "acronym cannot be null")
  @NotBlank(message = "acronym cannot be blank")
  private String acronym;

  @NotNull(message = "location cannot be null")
  @NotBlank(message = "acronym cannot be blank")
  private String location;

  @Pattern(regexp = "[01]", message = "status value must be '0' or '1'")
  private String status;
}
