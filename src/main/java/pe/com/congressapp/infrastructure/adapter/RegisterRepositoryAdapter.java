package pe.com.congressapp.infrastructure.adapter;

import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pe.com.congressapp.application.ports.output.UserRepositoryPort;
import pe.com.congressapp.domain.constants.RoleType;
import pe.com.congressapp.domain.entities.RoleEntity;
import pe.com.congressapp.domain.entities.UserEntity;
import pe.com.congressapp.domain.exception.ConflictException;
import pe.com.congressapp.domain.models.request.RegisterRequest;
import pe.com.congressapp.infrastructure.adapter.repository.RoleRepository;
import pe.com.congressapp.infrastructure.adapter.repository.UserRepository;
import pe.com.congressapp.infrastructure.mapper.UserMapper;

@Component
@RequiredArgsConstructor
public class RegisterRepositoryAdapter implements UserRepositoryPort {
  private final UserRepository userRepository;
  private final RoleRepository roleRepository;
  private final UserMapper userMapper;
  private final PasswordEncoder passwordEncoder;

  @Override
  public UserEntity saveUser(RegisterRequest request) {
    Set<RoleEntity> role = roleRepository.findByRoleType(RoleType.PARTICIPANT.getDescription());
    request.setPassword(passwordEncoder.encode(request.getPassword()));
    UserEntity userEntity = userMapper.toDomain(request);
    userEntity.setCongid(1L);
    userEntity.setRoles(role);
    userRepository
        .findByUsernameOrEmail(request.getUsername(), request.getEmail())
        .ifPresent(
            user -> {
              throw new ConflictException("User/Email already exists");
            });
    return userRepository.save(userEntity);
  }

  @Override
  public Optional<UserEntity> getUser(String usernameOrEmail) {
    return userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
  }
}
