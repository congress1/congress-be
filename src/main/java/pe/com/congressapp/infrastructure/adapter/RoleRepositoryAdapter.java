package pe.com.congressapp.infrastructure.adapter;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pe.com.congressapp.application.ports.output.RoleRepositoryPort;
import pe.com.congressapp.domain.Role;
import pe.com.congressapp.domain.entities.RoleEntity;
import pe.com.congressapp.domain.exception.ConflictException;
import pe.com.congressapp.domain.exception.NotFoundException;
import pe.com.congressapp.domain.models.request.RoleRequest;
import pe.com.congressapp.infrastructure.adapter.repository.RoleRepository;
import pe.com.congressapp.infrastructure.mapper.RoleMapper;
import pe.com.congressapp.infrastructure.utils.CongressUtils;
import pe.com.congressapp.infrastructure.utils.DateUtils;

@Component
@RequiredArgsConstructor
public class RoleRepositoryAdapter implements RoleRepositoryPort {

  private final RoleRepository roleRepository;

  private final RoleMapper roleMapper;

  @Override
  public List<Role> getListRoles() {
    return roleRepository.findAllByStatus(CongressUtils.STATUS_ACTIVE).stream()
        .map(roleMapper::toDomain)
        .toList();
  }

  @Override
  public Role saveRole(RoleRequest roleRequest) {

    assertNameNotExist(roleRequest.getName());

    RoleEntity roleEntity = roleMapper.toEntity(roleRequest);
    return roleMapper.toDomain(roleRepository.save(roleEntity));
  }

  @Override
  public Optional<Role> updateRole(RoleRequest roleRequest) {
    return roleRepository
        .findByIdAndStatus(roleRequest.getId(), CongressUtils.STATUS_ACTIVE)
        .map(
            roleEntity -> {
              roleEntity.setName(roleRequest.getName());
              roleEntity.setModifiedAt(DateUtils.now());
              return roleRepository.save(roleEntity);
            })
        .map(entity -> Optional.of(roleMapper.toDomain(entity)))
        .orElseThrow(() -> new NotFoundException("Role not found"));
  }

  @Override
  public void updateStatusRole(Long id) {
    roleRepository
        .findById(id)
        .ifPresentOrElse(
            roleEntity -> {
              String status = roleEntity.getStatus();
              roleRepository.updateStatusById(
                  id,
                  status.equalsIgnoreCase(CongressUtils.STATUS_ACTIVE)
                      ? CongressUtils.STATUS_INACTIVE
                      : CongressUtils.STATUS_ACTIVE);
            },
            () -> {
              throw new NotFoundException("Role not found: " + id);
            });
  }

  private void assertNameNotExist(String name) {
    roleRepository
        .findByName(name)
        .ifPresent(
            m -> {
              throw new ConflictException("Role name already exists: " + name);
            });
  }
}
