package pe.com.congressapp.infrastructure.adapter;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pe.com.congressapp.application.ports.output.SpeakerRepositoryPort;
import pe.com.congressapp.domain.Speaker;
import pe.com.congressapp.domain.entities.SpeakerEntity;
import pe.com.congressapp.domain.exception.ConflictException;
import pe.com.congressapp.domain.exception.NotFoundException;
import pe.com.congressapp.domain.models.request.SpeakerRequest;
import pe.com.congressapp.infrastructure.adapter.repository.SpeakerRepository;
import pe.com.congressapp.infrastructure.mapper.SpeakerMapper;
import pe.com.congressapp.infrastructure.utils.CongressUtils;
import pe.com.congressapp.infrastructure.utils.DateUtils;

@Component
@RequiredArgsConstructor
public class SpeakerRepositoryAdapter implements SpeakerRepositoryPort {

  private final SpeakerRepository speakerRepository;

  private final SpeakerMapper speakerMapper;

  @Override
  public List<Speaker> getListSpeakers() {
    return speakerRepository.findAllByStatus(CongressUtils.STATUS_ACTIVE).stream()
        .map(speakerMapper::toDomain)
        .toList();
  }

  @Override
  public Speaker saveSpeaker(SpeakerRequest speakerRequest) {

    assertNameNotExist(speakerRequest.getName(), speakerRequest.getLastName());

    SpeakerEntity speakerEntity = speakerMapper.toEntity(speakerRequest);
    return speakerMapper.toDomain(speakerRepository.save(speakerEntity));
  }

  @Override
  public Optional<Speaker> updateSpeaker(SpeakerRequest speakerRequest) {
    return speakerRepository
        .findByIdAndStatus(speakerRequest.getId(), CongressUtils.STATUS_ACTIVE)
        .map(
            speakerEntity -> {
              speakerEntity.setName(speakerRequest.getName());
              speakerEntity.setLastName(speakerRequest.getLastName());
              speakerEntity.setNationality(speakerRequest.getNationality());
              speakerEntity.setDescription(speakerRequest.getDescription());
              speakerEntity.setUrlSocialNetwork(speakerRequest.getUrlSocialNetwork());
              speakerEntity.setModifiedAt(DateUtils.now());
              return speakerRepository.save(speakerEntity);
            })
        .map(entity -> Optional.of(speakerMapper.toDomain(entity)))
        .orElseThrow(() -> new NotFoundException("Speaker not found"));
  }

  @Override
  public void updateStatusSpeaker(Long id) {
    speakerRepository
        .findById(id)
        .ifPresentOrElse(
            universityEntity -> {
              String status = universityEntity.getStatus();
              speakerRepository.updateStatusById(
                  id,
                  status.equalsIgnoreCase(CongressUtils.STATUS_ACTIVE)
                      ? CongressUtils.STATUS_INACTIVE
                      : CongressUtils.STATUS_ACTIVE);
            },
            () -> {
              throw new NotFoundException("Speaker not found: " + id);
            });
  }

  private void assertNameNotExist(String name, String lastName) {
    speakerRepository
        .findByNameAndLastName(name, lastName)
        .ifPresent(
            m -> {
              throw new ConflictException("Speaker name and lastName already exists: " + name);
            });
  }
}
