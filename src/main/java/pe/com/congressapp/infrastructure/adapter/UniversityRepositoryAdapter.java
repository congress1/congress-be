package pe.com.congressapp.infrastructure.adapter;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pe.com.congressapp.application.ports.output.UniversityRepositoryPort;
import pe.com.congressapp.domain.University;
import pe.com.congressapp.domain.entities.UniversityEntity;
import pe.com.congressapp.domain.exception.ConflictException;
import pe.com.congressapp.domain.exception.NotFoundException;
import pe.com.congressapp.domain.models.request.UniversityRequest;
import pe.com.congressapp.infrastructure.adapter.repository.UniversityRepository;
import pe.com.congressapp.infrastructure.mapper.UniversityMapper;
import pe.com.congressapp.infrastructure.utils.CongressUtils;
import pe.com.congressapp.infrastructure.utils.DateUtils;

@Component
@RequiredArgsConstructor
public class UniversityRepositoryAdapter implements UniversityRepositoryPort {

  private final UniversityRepository universityRepository;

  private final UniversityMapper universityMapper;

  @Override
  public List<University> getListUniversities() {
    return universityRepository.findAllByStatus(CongressUtils.STATUS_ACTIVE).stream()
        .map(universityMapper::toDomain)
        .toList();
  }

  @Override
  public University saveUniversity(UniversityRequest universityRequest) {

    assertNameNotExist(universityRequest.getName());

    UniversityEntity universityEntity = universityMapper.toEntity(universityRequest);
    return universityMapper.toDomain(universityRepository.save(universityEntity));
  }

  @Override
  public Optional<University> updateUniversity(UniversityRequest universityRequest) {
    return universityRepository
        .findByIdAndStatus(universityRequest.getId(), CongressUtils.STATUS_ACTIVE)
        .map(
            universityEntity -> {
              universityEntity.setName(universityRequest.getName());
              universityEntity.setAcronym(universityRequest.getAcronym());
              universityEntity.setLocation(universityRequest.getLocation());
              universityEntity.setModifiedAt(DateUtils.now());
              return universityRepository.save(universityEntity);
            })
        .map(entity -> Optional.of(universityMapper.toDomain(entity)))
        .orElseThrow(() -> new NotFoundException("University not found"));
  }

  @Override
  public void updateStatusUniversity(Long id) {
    universityRepository
        .findById(id)
        .ifPresentOrElse(
            universityEntity -> {
              String status = universityEntity.getStatus();
              universityRepository.updateStatusById(
                  id,
                  status.equalsIgnoreCase(CongressUtils.STATUS_ACTIVE)
                      ? CongressUtils.STATUS_INACTIVE
                      : CongressUtils.STATUS_ACTIVE);
            },
            () -> {
              throw new NotFoundException("University not found: " + id);
            });
  }

  private void assertNameNotExist(String name) {
    universityRepository
        .findByName(name)
        .ifPresent(
            m -> {
              throw new ConflictException("University name already exists: " + name);
            });
  }
}
