package pe.com.congressapp.infrastructure.adapter.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.com.congressapp.domain.entities.RoleEntity;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

  List<RoleEntity> findAllByStatus(String status);

  Optional<RoleEntity> findByName(String name);

  Set<RoleEntity> findByRoleType(String roleType);

  Optional<RoleEntity> findByIdAndStatus(Long id, String status);

  @Transactional
  @Modifying
  @Query("UPDATE RoleEntity r SET r.status = :status WHERE r.id = :id")
  void updateStatusById(Long id, String status);
}
