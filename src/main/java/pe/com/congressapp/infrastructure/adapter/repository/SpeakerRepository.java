package pe.com.congressapp.infrastructure.adapter.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.com.congressapp.domain.entities.SpeakerEntity;

@Repository
public interface SpeakerRepository extends JpaRepository<SpeakerEntity, Long> {

  List<SpeakerEntity> findAllByStatus(String status);

  Optional<SpeakerEntity> findByNameAndLastName(String name, String lastName);

  Optional<SpeakerEntity> findByIdAndStatus(Long id, String status);

  @Transactional
  @Modifying
  @Query("UPDATE SpeakerEntity s SET s.status = :status WHERE s.id = :id")
  void updateStatusById(Long id, String status);
}
