package pe.com.congressapp.infrastructure.adapter.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.com.congressapp.domain.entities.UniversityEntity;

@Repository
public interface UniversityRepository extends JpaRepository<UniversityEntity, Long> {

  List<UniversityEntity> findAllByStatus(String status);

  Optional<UniversityEntity> findByName(String name);

  Optional<UniversityEntity> findByIdAndStatus(Long id, String status);

  @Transactional
  @Modifying
  @Query("UPDATE UniversityEntity u SET u.status = :status WHERE u.id = :id")
  void updateStatusById(Long id, String status);
}
