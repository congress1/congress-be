package pe.com.congressapp.infrastructure.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.congressapp.application.ports.input.UserUseCase;
import pe.com.congressapp.domain.models.request.LoginRequest;
import pe.com.congressapp.domain.models.request.RegisterRequest;

@Slf4j
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
  private final UserUseCase registerUseCase;

  @PostMapping("login")
  public ResponseEntity<String> login(@Valid @RequestBody LoginRequest loginRequest) {
    return ResponseEntity.ok(registerUseCase.login(loginRequest));
  }

  @PostMapping("register")
  public ResponseEntity<String> register(@Valid @RequestBody RegisterRequest request) {
    return ResponseEntity.ok(registerUseCase.saveUser(request));
  }
}
