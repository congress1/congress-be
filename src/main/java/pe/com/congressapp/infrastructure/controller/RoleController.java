package pe.com.congressapp.infrastructure.controller;

import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.congressapp.application.ports.input.RoleUseCase;
import pe.com.congressapp.domain.Role;
import pe.com.congressapp.domain.models.request.RoleRequest;

@Slf4j
@RestController
@RequestMapping("/role")
@RequiredArgsConstructor
public class RoleController {

  private final RoleUseCase roleUseCase;

  @GetMapping
  public ResponseEntity<List<Role>> getListRoles() {
    return ResponseEntity.ok(roleUseCase.getRoles());
  }

  @PostMapping
  public ResponseEntity<Role> saveRole(@Valid @RequestBody RoleRequest roleRequest) {
    return new ResponseEntity<>(roleUseCase.saveRole(roleRequest), HttpStatus.CREATED);
  }

  @PutMapping
  public ResponseEntity<Role> updateRole(@Valid @RequestBody RoleRequest roleRequest) {
    return roleUseCase
        .updateRole(roleRequest)
        .map(role -> new ResponseEntity<>(role, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PutMapping("/{id}")
  public ResponseEntity<Void> updateStatusRole(@PathVariable Long id) {
    roleUseCase.updateStatusRole(id);
    return ResponseEntity.ok().build();
  }
}
