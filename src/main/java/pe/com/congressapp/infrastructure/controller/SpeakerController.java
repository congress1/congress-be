package pe.com.congressapp.infrastructure.controller;

import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.congressapp.application.ports.input.SpeakerUseCase;
import pe.com.congressapp.domain.Speaker;
import pe.com.congressapp.domain.models.request.SpeakerRequest;

@Slf4j
@RestController
@RequestMapping("/speaker")
@RequiredArgsConstructor
public class SpeakerController {

  private final SpeakerUseCase speakerUseCase;

  @GetMapping
  public ResponseEntity<List<Speaker>> getListSpeakers() {
    return ResponseEntity.ok(speakerUseCase.getSpeakers());
  }

  @PostMapping
  public ResponseEntity<Speaker> saveSpeaker(@Valid @RequestBody SpeakerRequest speakerRequest) {
    return new ResponseEntity<>(speakerUseCase.saveSpeaker(speakerRequest), HttpStatus.CREATED);
  }

  @PutMapping
  public ResponseEntity<Speaker> updateUniversity(
      @Valid @RequestBody SpeakerRequest speakerRequest) {
    return speakerUseCase
        .updateSpeaker(speakerRequest)
        .map(university -> new ResponseEntity<>(university, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PutMapping("/{id}")
  public ResponseEntity<Void> updateStatusUniversity(@PathVariable Long id) {
    speakerUseCase.updateStatusSpeaker(id);
    return ResponseEntity.ok().build();
  }
}
