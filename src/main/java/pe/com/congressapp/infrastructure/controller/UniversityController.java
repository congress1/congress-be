package pe.com.congressapp.infrastructure.controller;

import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.congressapp.application.ports.input.UniversityUseCase;
import pe.com.congressapp.domain.University;
import pe.com.congressapp.domain.models.request.UniversityRequest;

@Slf4j
@RestController
@RequestMapping("/university")
@RequiredArgsConstructor
public class UniversityController {

  private final UniversityUseCase universityUseCase;

  @GetMapping
  public ResponseEntity<List<University>> getListUniversities() {
    return ResponseEntity.ok(universityUseCase.getUniversities());
  }

  @PostMapping
  public ResponseEntity<University> saveUniversity(
      @Valid @RequestBody UniversityRequest universityRequest) {
    return new ResponseEntity<>(
        universityUseCase.saveUniversity(universityRequest), HttpStatus.CREATED);
  }

  @PutMapping
  public ResponseEntity<University> updateUniversity(
      @Valid @RequestBody UniversityRequest universityRequest) {
    return universityUseCase
        .updateUniversity(universityRequest)
        .map(university -> new ResponseEntity<>(university, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PutMapping("/{id}")
  public ResponseEntity<Void> updateStatusUniversity(@PathVariable Long id) {
    universityUseCase.updateStatusUniversity(id);
    return ResponseEntity.ok().build();
  }
}
