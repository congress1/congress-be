package pe.com.congressapp.infrastructure.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import pe.com.congressapp.domain.Role;
import pe.com.congressapp.domain.constants.RoleType;
import pe.com.congressapp.domain.entities.RoleEntity;
import pe.com.congressapp.domain.models.request.RoleRequest;

@Mapper(componentModel = "spring")
public interface RoleMapper {

  RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

  @BeanMapping(unmappedTargetPolicy = ReportingPolicy.IGNORE)
  @Mapping(source = "roleType", target = "type", qualifiedByName = "stringToRoleType")
  Role toDomain(RoleEntity roleEntity);

  @BeanMapping(unmappedTargetPolicy = ReportingPolicy.IGNORE)
  @Mapping(target = "createdAt", expression = "java(new java.util.Date())")
  @Mapping(target = "modifiedAt", expression = "java(new java.util.Date())")
  @Mapping(target = "status", constant = "1")
  @Mapping(source = "type", target = "roleType")
  RoleEntity toEntity(RoleRequest roleRequest);

  @Named("stringToRoleType")
  default RoleType stringToRoleType(String roleType) {
    return RoleType.getRoleType(roleType);
  }
}
