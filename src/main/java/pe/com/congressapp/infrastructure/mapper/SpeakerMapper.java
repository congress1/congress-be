package pe.com.congressapp.infrastructure.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import pe.com.congressapp.domain.Speaker;
import pe.com.congressapp.domain.entities.SpeakerEntity;
import pe.com.congressapp.domain.models.request.SpeakerRequest;

@Mapper(componentModel = "spring")
public interface SpeakerMapper {

  SpeakerMapper INSTANCE = Mappers.getMapper(SpeakerMapper.class);

  @BeanMapping(unmappedTargetPolicy = ReportingPolicy.IGNORE)
  Speaker toDomain(SpeakerEntity speakerEntity);

  @BeanMapping(unmappedTargetPolicy = ReportingPolicy.IGNORE)
  @Mapping(target = "createdAt", expression = "java(new java.util.Date())")
  @Mapping(target = "modifiedAt", expression = "java(new java.util.Date())")
  @Mapping(target = "status", constant = "1")
  SpeakerEntity toEntity(SpeakerRequest speakerRequest);
}
