package pe.com.congressapp.infrastructure.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import pe.com.congressapp.domain.University;
import pe.com.congressapp.domain.entities.UniversityEntity;
import pe.com.congressapp.domain.models.request.UniversityRequest;

@Mapper(componentModel = "spring")
public interface UniversityMapper {

  UniversityMapper INSTANCE = Mappers.getMapper(UniversityMapper.class);

  @BeanMapping(unmappedTargetPolicy = ReportingPolicy.IGNORE)
  University toDomain(UniversityEntity universityEntity);

  @BeanMapping(unmappedTargetPolicy = ReportingPolicy.IGNORE)
  @Mapping(target = "createdAt", expression = "java(new java.util.Date())")
  @Mapping(target = "modifiedAt", expression = "java(new java.util.Date())")
  @Mapping(target = "status", constant = "1")
  UniversityEntity toEntity(UniversityRequest universityRequest);
}
