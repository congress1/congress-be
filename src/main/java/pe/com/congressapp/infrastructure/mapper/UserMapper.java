package pe.com.congressapp.infrastructure.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import pe.com.congressapp.domain.entities.UserEntity;
import pe.com.congressapp.domain.models.request.RegisterRequest;

@Mapper(componentModel = "spring")
public interface UserMapper {
  UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

  @BeanMapping(unmappedTargetPolicy = ReportingPolicy.IGNORE)
  UserEntity toDomain(RegisterRequest registerRequest);
}
