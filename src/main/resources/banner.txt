
${AnsiColor.BLUE} _________                                                        _____
${AnsiColor.BLUE} \_   ___ \  ____   ____    ___________   ____   ______ ______   /  _  \ ______ ______
${AnsiColor.BLUE} /    \  \/ /  _ \ /    \  / ___\_  __ \_/ __ \ /  ___//  ___/  /  /_\  \\____ \\____ \
${AnsiColor.BLUE} \     \___(  <_> )   |  \/ /_/  >  | \/\  ___/ \___ \ \___ \  /    |    \  |_> >  |_> >
${AnsiColor.BLUE}  \______  /\____/|___|  /\___  /|__|    \___  >____  >____  > \____|__  /   __/|   __/
${AnsiColor.BLUE}         \/            \//_____/             \/     \/     \/          \/|__|   |__|

${AnsiColor.GREEN}:: Running Java           ${AnsiColor.DEFAULT}v${java.version}
${AnsiColor.GREEN}:: Running Spring Boot    ${AnsiColor.DEFAULT}v${spring-boot.version}
${AnsiStyle.NORMAL}